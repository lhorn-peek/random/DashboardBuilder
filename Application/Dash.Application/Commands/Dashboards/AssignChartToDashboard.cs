﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.Dashboards
{
    public class AssignChartToDashboard
    {
        public Guid DashboardId { get; set; }

        public string TemplateSectionViewId { get; set; }
    }
}
