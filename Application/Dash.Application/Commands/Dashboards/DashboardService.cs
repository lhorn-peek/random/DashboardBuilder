﻿using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.Designers;
using Dash.Core.Domain.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.Dashboards
{
    public class DashboardService
    {
        private IIdGenerator _idGenerator;
        private IDashboardRepository _dashboardRepository;
        private IQueryValidator _queryValidator;

        public DashboardService(IIdGenerator idGenerator, 
            IDashboardRepository dashboardRepository, 
            IQueryValidator queryValidator)
        {
            _idGenerator = idGenerator;
            _dashboardRepository = dashboardRepository;
            _queryValidator = queryValidator;
        }

        public void When(StartNewDashboard cmd)
        {
            var dashboardId = _idGenerator.CreateNext();

            var dashboard = new Dashboard(dashboardId, new DesignerId(cmd.DesignerId), new TemplateId(cmd.TemplateId));

            _dashboardRepository.Save(dashboard);
        }

        public void When(AssignChartToDashboard cmd)
        {
            var chartId = _idGenerator.CreateNext();

            var dashboard = _dashboardRepository.GetById(cmd.DashboardId);

            dashboard.AddNewChart(chartId, new TemplateSectionViewId(cmd.TemplateSectionViewId));

            _dashboardRepository.Save(dashboard);
        }

        public void When(SetChartQuery cmd)
        {
            var dashboard = _dashboardRepository.GetById(cmd.DashboardId);

            dashboard.SetChartQuery(cmd.ChartId, cmd.QueryText, _queryValidator);

            _dashboardRepository.Save(dashboard);
        }

        public void When(ChangeChartType cmd)
        {
            var dashboard = _dashboardRepository.GetById(cmd.DashboardId);

            dashboard.ChangeChartType(cmd.ChartId, cmd.ChartType);

            _dashboardRepository.Save(dashboard);
        }
    }
}
