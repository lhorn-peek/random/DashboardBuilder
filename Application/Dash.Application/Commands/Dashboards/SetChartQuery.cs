﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.Dashboards
{
    public class SetChartQuery
    {
        public Guid DashboardId { get; set; }

        public Guid ChartId { get; set; }

        public string QueryText { get; set; }
    }
}
