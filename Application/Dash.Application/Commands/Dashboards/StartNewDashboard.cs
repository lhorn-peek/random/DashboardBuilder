﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.Dashboards
{
    public class StartNewDashboard
    {
        public Guid DesignerId { get; set; }

        public Guid TemplateId { get; set; }
    }
}
