﻿using Dash.Core.Domain.DataSources;
using Dash.Core.Domain.Designers;
using Dash.Infrastructure.Services.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.DataSources
{
    public class DataSourceService
    {
        private IUploadedFileSaver _fileSaver; // <-- move class to Infrastructure
        private IIdGenerator _idGenerator;
        private IDesignerRepository _designerRepository;
        private IDataSourceRepository _dataSourceRepository;
        private IDataCollectionGenerator _dataCollectionGenerator;

        public DataSourceService(IUploadedFileSaver fileSaver, 
            IIdGenerator idGenerator, 
            IDesignerRepository designerRepository,
            IDataSourceRepository dataSourceRepository,
            IDataCollectionGenerator dataCollectionGenerator)
        {
            _fileSaver = fileSaver;
            _idGenerator = idGenerator;
            _designerRepository = designerRepository;
            _dataSourceRepository = dataSourceRepository;
            _dataCollectionGenerator = dataCollectionGenerator;
        }

        public void When(GenerateDataSource cmd)
        {
            var savedFileLocation = _fileSaver.SaveFile(cmd.UploadedFile, cmd.DesignerId);

            var designer = _designerRepository.GetById(cmd.DesignerId);


            var dataSource = new DataSource(_idGenerator.CreateNext(), savedFileLocation, designer);

            dataSource.GenerateDatabaseCollection(_dataCollectionGenerator);

            _dataSourceRepository.Save(dataSource);

            //this is temp...
            _designerRepository.Save(designer);
        }
    }
}
