﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.DataSources
{
    public class GenerateDataSource
    {
        public Guid DesignerId { get; set; }

        public byte[] UploadedFile { get; set; }
    }
}
