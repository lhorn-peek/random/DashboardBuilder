﻿using Dash.Core.Domain.Designers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands.Designers
{
    public class DesignerService
    {
        private IIdGenerator _idGenerator;
        private IDesignerRepository _designerRepository;

        public DesignerService(IIdGenerator idGenerator, IDesignerRepository designerRepository)
        {
            _idGenerator = idGenerator;
            _designerRepository = designerRepository;
        }


        public void When(NewDesigner cmd)
        {
            var state = new DesignerState() { 
                ApplicationUserId = cmd.ApplicationUserId,
                Id = _idGenerator.CreateNext()
            };

            var designer = new Designer(state);

            _designerRepository.Save(designer);
        }
    }
}
