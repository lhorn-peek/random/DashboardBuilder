﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Application.Commands
{
    public interface IIdGenerator
    {
        Guid CreateNext();
    }
}
