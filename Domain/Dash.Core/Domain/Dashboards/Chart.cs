﻿//using Dash.Core.Domain.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dash.Core.Domain.Dashboards
{
    public class Chart : IAggregate<ChartState>
    {
        private ChartState _state;

        public Chart(Guid id, string dataSourceQueryText, Dashboard dashboard, string sectionViewId)
        {
            _state = new ChartState() { Id = id, DataSourceQueryText = dataSourceQueryText, TemplateSectionViewId = sectionViewId };

            _state.Dashboard = dashboard.State;

        }

        internal void configureSettings(string chartType)
        {
            _state.Settings = new ChartSettings();
            _state.Settings.ChartType = chartType;
        }


        public ChartState State { get { return _state; } }

        public ChartState GetState()
        {
            return _state;
        }

        
    }
}
