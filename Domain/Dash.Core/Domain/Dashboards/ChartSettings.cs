﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Dashboards
{
    public class ChartSettings
    {
        public ChartSettings()
        {
            ChartType = "PIE";
        }

        public string ChartType { get; set; }
    }
}
