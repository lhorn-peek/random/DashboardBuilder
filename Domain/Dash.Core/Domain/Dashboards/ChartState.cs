﻿using Dash.Core.Domain.Dashboards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Dashboards
{
    public class ChartState : EntityStateBase
    {

        public ChartState()
        {

        }

        public string Heading { get; set; }

        public ChartSettings Settings { get;  set; }

        public string DataSourceQueryText { get; set; }

        public DashboardState Dashboard { get; set; }

        public string TemplateSectionViewId { get; set; }
    }


}
