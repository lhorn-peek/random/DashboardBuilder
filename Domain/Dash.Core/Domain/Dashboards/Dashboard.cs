﻿//using Dash.Core.Domain.Charts;
using Dash.Core.Domain.DataSources;
using Dash.Core.Domain.Designers;
using Dash.Core.Domain.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Dashboards
{
    public class Dashboard : IAggregate<DashboardState>
    {
        private DashboardState _state;

        public Dashboard(Guid id, DesignerId designerId, TemplateId templateId)
        {
            _state = new DashboardState { Id = id, DesignerId = designerId.Id, TemplateId = templateId.Id };
            
        }

        public void AddNewChart(Guid id, TemplateSectionViewId sectionViewId)
        {
            //get a default query
            string dataSetQueryText = ""; 

            var chart = new Chart(id, dataSetQueryText, this, sectionViewId.ViewId);

            _state.Charts.Add(chart.State);
        }

        public void SetChartQuery(Guid chartId, string queryText, IQueryValidator queryValidator)
        {
            var chart = _state.Charts.FirstOrDefault(c => c.Id == chartId);

            if (chart == null)
                throw new MemberAccessException("Cannot change the chart type. The chart doesnt exist.");

            if (queryValidator.IsValid(queryText) == false)
                throw new Exception("query is invalid.");

            chart.DataSourceQueryText = queryText;
        }

        public void ChangeChartType(Guid chartId, string chartType)
        {
            var chart = _state.Charts.FirstOrDefault(c => c.Id == chartId);

            if (chart == null)
                throw new MemberAccessException("Cannot change the chart type. The chart doesnt exist.");

            chart.Settings.ChartType = chartType;
        }

        public DashboardState State { get { return _state; } }

        public DashboardState GetState()
        {
            return _state;
        }


        public object GetObjState()
        {
            return _state;
        }
    }
}
