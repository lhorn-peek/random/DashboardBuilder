﻿//using Dash.Core.Domain.Charts;
using Dash.Core.Domain.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Dashboards
{
    public class DashboardState : EntityStateBase
    {
        public DashboardState()
        {
            Charts = new List<ChartState>();
        }

        public string Title { get; set; }

        public IList<ChartState> Charts { get; set; }

        public Guid DesignerId { get; set; }

        public Guid TemplateId { get; set; }
    }
}
