﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Dashboards
{
    public interface IDashboardRepository : IRepository<Dashboard, DashboardState>
    {

    }
}
