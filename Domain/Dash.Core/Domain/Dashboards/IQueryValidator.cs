﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dash.Core.Domain.Dashboards
{
    public interface IQueryValidator
    {
        bool IsValid(string queryText);
    }
}
