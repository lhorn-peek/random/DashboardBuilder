﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.DataSources
{
    internal static class AllowedFileTypes
    {
        public static DatabaseType CorrespondingDatabase(string pathToFile)
        {
            DatabaseType dbType;

            if (pathToFile.EndsWith(".json"))
                dbType = DatabaseType.MongoDB;
            else if (pathToFile.EndsWith(".csv"))
                dbType = DatabaseType.SQL;
            else
                throw new ArgumentException("File type not supported");

            return dbType;
        }
    }

    public enum DatabaseType
    {
        SQL,
        MongoDB
    }
}
