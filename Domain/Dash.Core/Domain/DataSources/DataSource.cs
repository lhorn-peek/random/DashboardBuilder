﻿using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.Designers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.DataSources
{
    public class DataSource : IAggregate<DataSourceState>
    {
        private DataSourceState _state;

        private List<string> _brokenRules = new List<string>();


        public DataSource(Guid id, string sourceFilePath, Designer designer)
        {
            _state = new DataSourceState() { Id = id, SourceFilePath = sourceFilePath };

            _state.DesignerId = designer.State.Id;

            setDatabaseType(_state.SourceFilePath);

            designer.addDataSource(_state.Id); //move to domain service / Synchronous domain event 

            _state.CollectionStatus = CollectionStatus.None;
        }

        public void GenerateDatabaseCollection(IDataCollectionGenerator generator)
        {
            if (_state.CollectionStatus == CollectionStatus.Created)
                return;

            setUniqueCollectionName();

            generator.Generate(_state.DatabaseCollectionName, _state.DatabaseType, _state.SourceFilePath);

            _state.CollectionStatus = CollectionStatus.Created;

            setDefaultCollectionQuery();

        }

        public void AppendDatabaseCollection(string pathToAnotherFile)
        {
            throw new NotImplementedException();
        }

        private void setDatabaseType(string sourceFilePath)
        {
            _state.DatabaseType = AllowedFileTypes.CorrespondingDatabase(sourceFilePath);
        }

        private void setUniqueCollectionName()
        {
            var path = _state.SourceFilePath;
            var fileExtIndexStartsAt = path.IndexOf(".");

            var newName = String.Format("{0}_{1}",
                path.Substring(0, fileExtIndexStartsAt),
                DateTime.Now.ToString());

            _state.DatabaseCollectionName = newName;
        }

        private void setDefaultCollectionQuery()
        {
            if (_state.DatabaseType == DatabaseType.SQL)
                _state.DefaultCollectionQuery = "";
            else if (_state.DatabaseType == DatabaseType.MongoDB)
                _state.DefaultCollectionQuery = "";
        }

        public DataSourceState State { get { return _state; } }

        public DataSourceState GetState()
        {
            return _state;
        }
    }
}
