﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dash.Core.Domain.DataSources
{
    public class DataSourceId
    {
        public DataSourceId(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
