﻿using Dash.Core.Domain.Designers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.DataSources
{
    public class DataSourceState : EntityStateBase
    {

        public string SourceFilePath { get; set; }

        public DatabaseType DatabaseType { get; set; }

        public string DatabaseCollectionName { get; set; }

        public string DefaultCollectionQuery { get; set; }

        public CollectionStatus CollectionStatus { get; set; }

        public Guid DesignerId { get; set; }

    }

    public enum CollectionStatus
    {
        None,
        Created
    }
}
