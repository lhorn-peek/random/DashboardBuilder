﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dash.Core.Domain.DataSources
{
    public interface IDataCollectionGenerator
    {
        void Generate(string collectionName, DatabaseType type, string pathToSourceFile);
    }
}
