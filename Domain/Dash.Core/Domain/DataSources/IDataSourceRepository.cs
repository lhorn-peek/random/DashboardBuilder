﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.DataSources
{
    public interface IDataSourceRepository : IRepository<DataSource, DataSourceState>
    {
    }
}
