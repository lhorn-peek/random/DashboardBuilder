﻿using Dash.Core.Domain.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Designers
{
    public class Designer : IAggregate<DesignerState>
    {
        private DesignerState _state;

        public Designer(DesignerState state)
        {
            _state = state;
        }

        internal void addDataSource(Guid dataSourceId)
        {

            _state.DataSources.Add(dataSourceId);
        }

        public DesignerState State { get { return _state; } }

        public DesignerState GetState()
        {
            return _state;
        }
    }
}
