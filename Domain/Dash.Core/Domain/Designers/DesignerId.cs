﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dash.Core.Domain.Designers
{
    public class DesignerId
    {
        public DesignerId(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
