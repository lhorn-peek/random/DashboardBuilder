﻿using Dash.Core.Domain.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Designers
{
    public class DesignerState : EntityStateBase
    {
        public DesignerState()
        {
            DataSources = new List<Guid>();
        }

        public string ApplicationUserId { get; set; }

        public IList<Guid> DataSources { get; set; }
    }
}
