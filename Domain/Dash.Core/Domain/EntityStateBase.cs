﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain
{
    public abstract class EntityStateBase
    {
        public Guid Id { get; set; }
    }
}
