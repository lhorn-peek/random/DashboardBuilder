﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain
{
    public interface IAggregate<TRootEntity> where TRootEntity : EntityStateBase
    {
        TRootEntity GetState();

        TRootEntity State { get; }
    }
}
