﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain
{
    public interface IRepository<TAggregate, TEntityState> 
        where TAggregate : IAggregate<TEntityState> 
        where TEntityState : EntityStateBase
    {
        TAggregate GetById(object id);

        void Save(TAggregate aggregate);

    }
}
