﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Templates
{
    public class Section
    {
        public Guid Id { get; set; }

        public string ViewId { get; set; }
    }
}
