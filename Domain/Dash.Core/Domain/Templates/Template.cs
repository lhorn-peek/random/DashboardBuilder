﻿using Dash.Core.Domain.Designers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Templates
{
    public class Template : IAggregate<TemplateState>
    {
        private TemplateState _state;

        public Template(Guid id, DesignerId designerId)
        {
            _state = new TemplateState() { Id = id, DesignerId = designerId.Id };
        }


        public TemplateState State { get { return _state; } }

        public TemplateState GetState()
        {
            return _state;
        }
        //more logic here...
    }
}
