﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Templates
{
    public class TemplateId
    {
        public TemplateId(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; private set; }
    }
}
