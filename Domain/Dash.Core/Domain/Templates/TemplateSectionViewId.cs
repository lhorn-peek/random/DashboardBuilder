﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Templates
{
    public class TemplateSectionViewId
    {
        public TemplateSectionViewId(string viewId)
        {
            ViewId = viewId;
        }

        public string ViewId { get; private set; }
    }
}
