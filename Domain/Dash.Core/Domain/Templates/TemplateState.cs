﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Core.Domain.Templates
{
    public class TemplateState : EntityStateBase
    {
        public TemplateState()
        {
            Sections = new List<Section>();
        }

        public string Name { get; set; }

        public string View { get; set; }

        public List<Section> Sections { get; set; }

        public Guid DesignerId { get; set; }

    }

}
