﻿using Dash.Core.Domain.Dashboards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Data.Dashboards
{
    public class DashboardRepository : EfRepository<Dashboard,DashboardState>, IDashboardRepository
    {
        public DashboardRepository(IDbContext context) 
            : base(context)
        {
            
        }



    }
}
