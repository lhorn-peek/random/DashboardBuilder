﻿using Dash.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Data
{
    public class EfRepository<TAggregate, TEntityState> : IRepository<TAggregate, TEntityState> 

        where TAggregate : IAggregate<TEntityState>
        where TEntityState : EntityStateBase
    {

        #region fields

        private IDbContext _context;

        //private IDbSet<TState> _entities;  

        #endregion

        #region ctor

        public EfRepository(IDbContext context)
        {
            this._context = context;

            /* Put in check to make sure typeof 'TAggregate.GetState()' equals typeof 'TState'

               logic here... */
        }

        #endregion

        public TAggregate GetById(object id)
        {
            throw new NotImplementedException();
        }

        public void Save(TAggregate aggregate)
        {
            throw new NotImplementedException();
        }


        //public virtual IDbSet<TState> Entities
        //{
        //    get
        //    {
        //        if (_entities == null)
        //            _entities = _context.Set<TState>();
                
        //        return _entities;
        //    }
        //}
    }
}
