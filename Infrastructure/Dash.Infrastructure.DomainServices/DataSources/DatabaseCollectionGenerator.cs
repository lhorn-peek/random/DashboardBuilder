﻿using Dash.Core.Domain.DataSources;
using Dash.Infrastructure.Services.Database;
using Dash.Infrastructure.Services.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.DomainServices.DataSources
{
    public class DatabaseCollectionGenerator : IDataCollectionGenerator
    {
        #region fields

        private ITableSchemaGenerator _schemaGenerator;
        private ISqlDataImporter _sqlManager;
        private IMongoDataImporter _mongoManager;

        #endregion

        #region ctor

        public DatabaseCollectionGenerator(
           ISqlDataImporter sqlManager,
           IMongoDataImporter mongoManager,
           ITableSchemaGenerator schemaGenerator)
        {
            _sqlManager = sqlManager;
            _mongoManager = mongoManager;
            _schemaGenerator = schemaGenerator;
        } 

        #endregion

        public void Generate(string collectionName, DatabaseType type, string pathToSourceFile)
        {
            if (type == DatabaseType.SQL)
            {
                createSqlTable(pathToSourceFile, collectionName);
            }
            else if (type == DatabaseType.MongoDB)
            {
                createMongoCollection(pathToSourceFile, collectionName);
            }
                
        }

        private void createSqlTable(string pathToSourceFile, string collectionName)
        {
            var columnDefinitions = _schemaGenerator.DefineColumnDataTypes(pathToSourceFile);

            var fileCopy = _schemaGenerator.getWorkingCopyOfFile(pathToSourceFile);

            _sqlManager.CreateDatabaseCollection(collectionName, fileCopy, columnDefinitions);
        }

        private void createMongoCollection(string pathToSourceFile, string collectionName)
        {
            throw new NotImplementedException();
        }

    }
}
