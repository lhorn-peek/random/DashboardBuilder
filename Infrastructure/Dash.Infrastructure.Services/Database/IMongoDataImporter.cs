﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.Database
{
    public interface IMongoDataImporter 
    {
        void CreateDatabaseCollection(string collectionName, string pathToSourceFile);
    }
}
