﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.Database
{
    public interface ISqlDataImporter 
    {
        void CreateDatabaseCollection(string tableName, string pathToFile, Dictionary<string, Type> columnDefinitions);

        void initDestinationTable(string tableName, Dictionary<string, Type> columnDefinitions);
    }
}
