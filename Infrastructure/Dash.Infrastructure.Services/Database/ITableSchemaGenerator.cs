﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.Database
{
    public interface ITableSchemaGenerator
    {
        Dictionary<string, Type> DefineColumnDataTypes(string pathToFile);

        string getWorkingCopyOfFile(string pathToFile);
    }
}
