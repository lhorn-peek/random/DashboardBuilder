﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.Database
{
    public class SqlDataImporter : ISqlDataImporter
    {
        private readonly string _connectionString;

        public SqlDataImporter(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void CreateDatabaseCollection(string tableName, string pathToFileCopy, Dictionary<string, Type> columnDefinitions)
        {
            initDestinationTable(tableName, columnDefinitions);

            using (var reader = new CsvReader(new StreamReader(pathToFileCopy), true))
            {
                foreach (KeyValuePair<string, Type> item in columnDefinitions)
                    reader.Columns.Add(new Column() { Name = item.Key, Type = item.Value });
            
                using (var sbc = new SqlBulkCopy(_connectionString))
                {
                    sbc.DestinationTableName = String.Format("dbo.{0}", tableName);
                    sbc.BatchSize = 1000;

                    sbc.WriteToServer(reader);
                }
            }
        }

        public void initDestinationTable(string tableName, Dictionary<string, Type> columnDefinitions)
        {
            var sqlCon = new SqlConnection(_connectionString);

            try
            {
                sqlCon.Open();

                object exists = null;

                SqlCommand cmd = new SqlCommand("SELECT * FROM sysobjects where name = '" + tableName + "'", sqlCon);
                exists = cmd.ExecuteScalar();

                if (exists != null)
                    throw new OperationCanceledException("destination table already exists.");


                var commandBuilder = new StringBuilder("CREATE TABLE " + tableName + "(");

                foreach (KeyValuePair<string, Type> col in columnDefinitions)
                {
                    commandBuilder.Append(col.Key + " ");

                    if (col.Value == typeof(string))
                    {
                        commandBuilder.Append("VARCHAR(500),");
                    }
                    else if (col.Value == typeof(int))
                    {
                        commandBuilder.Append("INT,");
                    }
                    else if (col.Value == typeof(double))
                    {
                        commandBuilder.Append("FLOAT,");
                    }
                }

                commandBuilder.Remove(commandBuilder.Length - 1, 1);
                commandBuilder.Append(")");

                var createCommand = new SqlCommand(commandBuilder.ToString(), sqlCon);

                createCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("error creating table.", ex);
            }
            finally
            {
                sqlCon.Close();
            }
            
        }


    }
}
