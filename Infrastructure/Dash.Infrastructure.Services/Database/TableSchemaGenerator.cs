﻿using Dash.Infrastructure.Services.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.Database
{
    public class TableSchemaGenerator : ITableSchemaGenerator
    {
        #region fields

        private string[] _columnNames;

        private string[] _columnDataTypes;

        private ICsvReader _csvReader;

        private const int _columnsLineNumber = 1;

        private const int _dataTypesLineNumber = 2;

        private string _dataTypesLine;

        #endregion

        public TableSchemaGenerator(ICsvReader csvReader)
        {
            _csvReader = csvReader;
        }

        public Dictionary<string, Type> DefineColumnDataTypes(string pathToFile)
        {
            getColumnsAndTypesFromFile(pathToFile);

            var columnDefinitions = new Dictionary<string, Type>();

            for (int i = 0; i < _columnNames.Length; i++)
            {
                var type = Type.GetType(String.Format("System.{0}", _columnDataTypes[i]));

                columnDefinitions.Add(_columnNames[i], type);
            }

            return columnDefinitions;
        }

        public string getWorkingCopyOfFile(string pathToFile)
        {

            try
            {
                var currentPath = pathToFile.Substring(0, pathToFile.LastIndexOf('\\') + 1);

                var workingCopyFileName = String.Format("{0}_{1}", Path.GetFileNameWithoutExtension(pathToFile), "working.csv");


                string newFile = String.Format("{0}{1}", currentPath, workingCopyFileName);

                File.Copy(pathToFile, newFile, true);


                var tempFile = Path.GetTempFileName();

                var linesToKeep = File.ReadLines(newFile).Where(l => l != _dataTypesLine);

                File.WriteAllLines(tempFile, linesToKeep);

                File.Delete(newFile);

                File.Move(tempFile, newFile);


                return newFile;
            }
            catch (Exception ex)
            {

                throw new Exception("Error creating working copy of file '" + pathToFile + "'", ex);
            }
        }

        private void getColumnsAndTypesFromFile(string pathToFile)
        {
            _columnNames = _csvReader.GetColumnsLine(pathToFile, _columnsLineNumber).Split(',');

            _dataTypesLine = _csvReader.GetDataTypesLine(pathToFile, _dataTypesLineNumber);

            _columnDataTypes = _dataTypesLine.Split(',');

            if (_columnNames.Length != _columnDataTypes.Length)
                throw new Exception("columns line and data types line are not same length.");
        }

    }
}
