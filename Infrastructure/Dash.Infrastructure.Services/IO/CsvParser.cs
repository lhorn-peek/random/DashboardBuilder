﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GenericParsing;
using System.IO;
using LumenWorks.Framework.IO.Csv;

namespace Dash.Infrastructure.Services.IO
{
    public class CsvParser 
    {
        private string _collectionName;

        private readonly string[] allowedDataTypes = new string[] { "String", "Double", "Int32" };

        private string[] _dataTypesStrArray;

        private string _dataTypesLine;

        private List<Type> _dataTypes = new List<Type>();

        private string[] _columnNames;
        private string _columnsLine;

        public DataTable ParseCsv(string pathToFile, string collectionName)
        {
            _collectionName = collectionName;

            getDataTypesLine(pathToFile);
            setDataTypes();

            var columns = setColumnStructure();

            removeDataTypesLine(pathToFile);

            using (var csv = new CachedCsvReader(new StreamReader(pathToFile), true))
            {
                foreach (var column in columns)
                    csv.Columns.Add(column);

                var dataTable = new DataTable(_collectionName);
                dataTable.Load(csv);
                
                return dataTable;
            }

        }

        private void setDataTypes()
        {
            foreach (var dataType in _dataTypesStrArray)
            {
                if (!allowedDataTypes.Contains(dataType))
                    throw new Exception("data type not supported.");

                var type = Type.GetType(String.Format("System.{0}", dataType));

                _dataTypes.Add(type);
            }
        }

        private List<Column> setColumnStructure()
        {
            var columns = new List<Column>();

            var i = 0;

            foreach (var columnName in _columnNames)
            {
                columns.Add(new Column() { Name = columnName, Type = _dataTypes[i] });
                i++;
            }
                

            return columns;
        }

        private void removeDataTypesLine(string fileName)
        {
            var tempFile = Path.GetTempFileName();

            //File.Copy(fileName, Path.GetFileNameWithoutExtension(fileName) + "_original");

            var linesToKeep = File.ReadLines(fileName).Where(l => l != _dataTypesLine);

            File.WriteAllLines(tempFile, linesToKeep);

            File.Delete(fileName);
            File.Move(tempFile, fileName);
        }

        private void getDataTypesLine(string pathToFile)
        {
            string dataTypesLine;
            string columnsLine = "";

            using (TextReader reader = File.OpenText(pathToFile))
            {
                int lineCount = 1;
                while (true)
                {
                    var line = reader.ReadLine();

                    if (line == null)
                    {
                        reader.Close();
                        throw new Exception("incorrect structure in supplied .csv file");
                    }

                    if (lineCount == 1)
                    {
                        columnsLine = line;
                        
                    }
                    else if (lineCount == 2)
                    {
                        dataTypesLine = line;
                        break;
                    }

                    lineCount++;
                }

                reader.Close();
            }

            _dataTypesLine = dataTypesLine;
            _dataTypesStrArray = dataTypesLine.Split(',');
            _columnNames = columnsLine.Split(',');
            _columnsLine = columnsLine;

        }
    }
}
