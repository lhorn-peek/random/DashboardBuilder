﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.IO
{
    public class CsvReader : ICsvReader
    {
        public string GetColumnsLine(string pathToFile, int lineNumber)
        {
            if (lineNumber < 1)
                throw new ArgumentException("lineNumber must be equal to or greater than 1.");

            string columnsLine;

            using (TextReader reader = File.OpenText(pathToFile))
            {
                int lineCount = 1;
                while (true)
                {
                    var line = reader.ReadLine();

                    if (line == null)
                    {
                        reader.Close();
                        throw new Exception("incorrect structure in supplied .csv file");
                    }

                    if (lineCount == lineNumber)
                    {
                        columnsLine = line;
                        break;

                    }

                    lineCount++;
                }

                reader.Close();
            }

            return columnsLine;
        }

        public string GetDataTypesLine(string pathToFile, int lineNumber)
        {
            if (lineNumber < 1)
                throw new ArgumentException("lineNumber must be equal to or greater than 1.");

            string dataTypesLine;

            using (TextReader reader = File.OpenText(pathToFile))
            {
                int lineCount = 1;
                while (true)
                {
                    var line = reader.ReadLine();

                    if (line == null)
                    {
                        reader.Close();
                        throw new Exception("incorrect structure in supplied .csv file");
                    }

                    if (lineCount == lineNumber)
                    {
                        dataTypesLine = line;
                        break;

                    }

                    lineCount++;
                }

                reader.Close();
            }

            return dataTypesLine;
        }
    }
}
