﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.IO
{
    public interface ICsvReader
    {
        string GetColumnsLine(string pathToFile, int lineNumber);

        string GetDataTypesLine(string pathToFile, int lineNumber);
    }
}
