﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Infrastructure.Services.IO
{
    public interface IUploadedFileSaver
    {
        string SaveFile(byte[] file, Guid DesignerId);
    }
}
