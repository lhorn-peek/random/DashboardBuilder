# DashboardBuilder

DashboardBuilder follows Domain-Driven-Design (DDD) techniques.

note: still in concept and design phase!

An application where developers can design simple dashboards by running the application locally on their computer. The idea is that a dev can start a new dashboard, choose a template, and then assign a chart to each dashboard template section and eventually publish the dashboard online and share it with the appropriate people.

Where it gets interesting is when the dev needs to supply data to each chart. The idea is to let the dev assign a database query to each chart. The query can be a SQL query or a mongodb query such as a MongoDB Aggregation Framework Script. 

A dev can supply his/her own database (since it is run locally), but the main idea is to let a dev upload a file, either a .CSV or .JSON file. Once uploaded the system imports the data into a database. 

If it is a .CSV file, then the system will read the file, create the table schema in MSSQL and import the data into the default database. The same applies to .JSON files. Json files will be imported into a MongoDB Collection.
