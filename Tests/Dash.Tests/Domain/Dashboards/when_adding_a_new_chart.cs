﻿using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.Designers;
using Dash.Core.Domain.Templates;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Tests.Domain.Dashboards
{
    public class when_adding_a_new_chart : SpecificationBase
    {
        private Dashboard sut;
        private Guid id;
        private Guid chartId;
        private DesignerId designerId;
        private TemplateId templateId;

        protected override void Given()
        {
            id = Guid.NewGuid();
            chartId = Guid.NewGuid();
            designerId = new DesignerId(Guid.NewGuid());
            templateId = new TemplateId(Guid.NewGuid());

             sut = new Dashboard(id, designerId, templateId);
        }

        protected override void When()
        {
            sut.AddNewChart(chartId, new TemplateSectionViewId("a_id_for_chart_container_on_view"));
        }

        [Then]
        public void it_should_be_in_the_state()
        {
            var state = sut.State;

            Assert.That(state.Charts.Count, Is.GreaterThan(0));
        }

        [Then]
        public void it_should_have_a_id()
        {
            var chart = sut.State.Charts.FirstOrDefault();

            Assert.That(chart.Id, Is.EqualTo(chartId));
        }

        [Then]
        public void it_should_have_a_template_section_id()
        {
            var chart = sut.State.Charts.FirstOrDefault();

            Assert.That(chart.TemplateSectionViewId, Is.Not.Empty);
        }

        [Then]
        public void it_should_have_a_ref_to_dashboard()
        {
            var chart = sut.State.Charts.FirstOrDefault();

            Assert.That(chart.Dashboard, Is.EqualTo(sut.State));
        }
    }
}
