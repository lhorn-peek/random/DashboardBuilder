﻿using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.Designers;
using Dash.Core.Domain.Templates;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Tests.Domain.Dashboards
{
    public class when_creating_a_new_dashboard : SpecificationBase
    {
        private Dashboard sut;
        private Guid id;
        private DesignerId designerId;
        private TemplateId templateId;

        protected override void Given()
        {
            id = Guid.NewGuid();
            designerId = new DesignerId(Guid.NewGuid());
            templateId = new TemplateId(Guid.NewGuid());

        }

        protected override void When()
        {
            sut = new Dashboard(id, designerId, templateId);
        }

        [Then]
        public void it_should_set_the_id_of_the_state()
        {
            var state = sut.State;

            Assert.That(state.Id, Is.EqualTo(id));
        }

        [Then]
        public void it_should_set_the_designerId_of_the_state()
        {
            var state = sut.State;

            Assert.That(state.DesignerId, Is.EqualTo(designerId.Id));
        }

        [Then]
        public void it_should_set_the_templateId_of_the_state()
        {
            var state = sut.State;

            Assert.That(state.TemplateId, Is.EqualTo(templateId.Id));
        }
    }
}
