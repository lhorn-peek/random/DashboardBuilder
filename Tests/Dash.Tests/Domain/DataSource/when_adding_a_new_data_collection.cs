﻿using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.DataSources;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Tests.Domain.DataSource
{
    public class when_initializing_a_new_data_collection : SpecificationBase
    {
        private DataSource sut;
        private Guid id;
        private string sourceFilePath;
        private Dashboard dashboard;


        protected override void Given()
        {
            dashboard = new Dashboard(Guid.NewGuid(), "title");

            id = Guid.NewGuid();
            sourceFilePath = @"c:\dashbuilder\sourceFiles\james_ah33546mdbb\sales.csv";

            sut = new DataSource(id, sourceFilePath, dashboard);
        }

        protected override void When()
        {
            var fakeDataCollectionGenerator = new Mock<IDataCollectionGenerator>();

            sut.InitializeDataCollection(fakeDataCollectionGenerator.Object);
        }

        [Then]
        public void there_should_be_a_new_data_collection_in_state()
        {
            var state = sut.State;

            Assert.That(state.DataCollection, Is.Not.Null);
        }

        [Then]
        public void the_state_should_contain_the_new_collection_name()
        {
            var state = sut.State;

            Assert.That(state.DataCollection.Name, Is.Not.Empty);
        }

        
    }
}
