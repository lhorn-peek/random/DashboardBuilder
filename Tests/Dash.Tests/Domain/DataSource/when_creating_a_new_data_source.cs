﻿using Dash.Core.Domain.DataSources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Dash.Core.Domain.Dashboards;
using Dash.Core.Domain.Designers;

namespace Dash.Tests.Domain.DataSources
{
    public class when_creating_a_new_data_source : SpecificationBase
    {
        private Guid id;
        private string sourceFilePath;
        private DatabaseType expectedType;
        private Designer designer;
        private DataSource sut;

        protected override void Given()
        {
            id = Guid.NewGuid();
            sourceFilePath = @"c:\fakePathToSomeFile\file.csv";
            expectedType = DatabaseType.SQL;
            designer = new Designer(new DesignerState() { Id = Guid.NewGuid(), ApplicationUserId = "abc123" });
        }

        protected override void When()
        {
            sut = new DataSource(id, sourceFilePath, designer);
        }

        [Then]
        public void the_state_should_contain_an_id()
        {
            var state = sut.State;

            Assert.That(state.Id, Is.EqualTo(id));
        }

        [Then]
        public void it_have_a_path_to_a_source_file()
        {
            var state = sut.State;

            Assert.That(state.SourceFilePath, Is.EqualTo(sourceFilePath));
        }

        [Then]
        public void the_databaseType_should_be_in_state()
        {
            var state = sut.State;

            Assert.That(state.DatabaseType, Is.EqualTo(expectedType));
        }

        [Then]
        public void the_collection_status_should_be_set_to_NONE()
        {
            var state = sut.State;

            Assert.That(state.CollectionStatus, Is.EqualTo(CollectionStatus.None));
        }

        [Then]
        public void the_designer_datasources_is_not_empty()
        {

            Assert.That(designer.State.DataSources, Is.Not.Empty);
        }

        [Then]
        public void the_datasource_should_have_a_ref_to_designer()
        {
            var state = sut.State;

            Assert.That(state.DesignerId, Is.EqualTo(designer.State.Id));
        }
    }
}
