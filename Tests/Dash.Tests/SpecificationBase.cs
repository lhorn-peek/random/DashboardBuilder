﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dash.Tests
{
    [TestFixture]
    public abstract class SpecificationBase
    {
        [SetUp]
        public void SetUp()
        {
            SetupGraphDependencies();

            Given();
            When();
        }

        protected virtual void SetupGraphDependencies() { }

        protected virtual void Given() { }
        protected virtual void When() { }
    }

    public class ThenAttribute : TestAttribute { }
}
